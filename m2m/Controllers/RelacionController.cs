using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using m2m.Data;
using m2m.Models;

namespace m2m.Controllers
{
    public class RelacionController : Controller
    {
        private readonly AAContext _context;

        public RelacionController(AAContext context)
        {
            _context = context;    
        }

        // GET: Relacion
        public async Task<IActionResult> Index()
        {
            var aAContext = _context.AlumnoAula.Include(a => a.Alumno).Include(a => a.Aula);
            return View(await aAContext.ToListAsync());
        }

        // GET: Relacion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnoAula = await _context.AlumnoAula
                .Include(a => a.Alumno)
                .Include(a => a.Aula)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (alumnoAula == null)
            {
                return NotFound();
            }

            return View(alumnoAula);
        }

        // GET: Relacion/Create
        public IActionResult Create()
        {
            ViewData["AlumnoId"] = new SelectList(_context.Alumnos, "Id", "Nombre");
            ViewData["AulaId"] = new SelectList(_context.Aula, "Id", "Nombre");
            return View();
        }

        // POST: Relacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,AlumnoId,AulaId")] AlumnoAula alumnoAula)
        {
            if (ModelState.IsValid)
            {
                _context.Add(alumnoAula);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["AlumnoId"] = new SelectList(_context.Alumnos, "Id", "Nombre", alumnoAula.AlumnoId);
            ViewData["AulaId"] = new SelectList(_context.Aula, "Id", "Nombre", alumnoAula.AulaId);
            return View(alumnoAula);
        }

        // GET: Relacion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnoAula = await _context.AlumnoAula.SingleOrDefaultAsync(m => m.Id == id);
            if (alumnoAula == null)
            {
                return NotFound();
            }
            ViewData["AlumnoId"] = new SelectList(_context.Alumnos, "Id", "Nombre", alumnoAula.AlumnoId);
            ViewData["AulaId"] = new SelectList(_context.Aula, "Id", "Nombre", alumnoAula.AulaId);
            return View(alumnoAula);
        }

        // POST: Relacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AlumnoId,AulaId")] AlumnoAula alumnoAula)
        {
            if (id != alumnoAula.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alumnoAula);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlumnoAulaExists(alumnoAula.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AlumnoId"] = new SelectList(_context.Alumnos, "Id", "Nombre", alumnoAula.AlumnoId);
            ViewData["AulaId"] = new SelectList(_context.Aula, "Id", "Nombre", alumnoAula.AulaId);
            return View(alumnoAula);
        }

        // GET: Relacion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnoAula = await _context.AlumnoAula
                .Include(a => a.Alumno)
                .Include(a => a.Aula)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (alumnoAula == null)
            {
                return NotFound();
            }

            return View(alumnoAula);
        }

        // POST: Relacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var alumnoAula = await _context.AlumnoAula.SingleOrDefaultAsync(m => m.Id == id);
            _context.AlumnoAula.Remove(alumnoAula);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AlumnoAulaExists(int id)
        {
            return _context.AlumnoAula.Any(e => e.Id == id);
        }
    }
}
