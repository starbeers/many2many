﻿using m2m.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2m.Data
{
    public class AAContext:DbContext
    {
        public AAContext(DbContextOptions options): base(options)  {}

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<AlumnoAula>()
        //        .HasKey(aa => new { aa.AlumnoId, aa.AulaId });

        //    modelBuilder.Entity<AlumnoAula>()
        //        .HasOne(aa => aa.Alumno)
        //        //.WithMany("Aulas")
        //        .WithMany(a => a.Aulas)
        //        .HasForeignKey(aa => aa.AlumnoId);

        //    modelBuilder.Entity<AlumnoAula>()
        //        .HasOne(aa => aa.Aula)
        //        //.WithMany("Alumnos")
        //        .WithMany(a => a.Alumnos)
        //        .HasForeignKey(aa => aa.AulaId);
        //}

        public DbSet<Alumno> Alumnos { get; set; }
        public DbSet<Aula> Aula { get; set; }
        public DbSet<AlumnoAula> AlumnoAula { get; set; }
    }
}
