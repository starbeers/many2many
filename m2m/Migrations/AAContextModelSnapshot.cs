﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using m2m.Data;

namespace m2m.Migrations
{
    [DbContext(typeof(AAContext))]
    partial class AAContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("m2m.Models.Alumno", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Edad");

                    b.Property<string>("Nombre")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Alumnos");
                });

            modelBuilder.Entity("m2m.Models.AlumnoAula", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AlumnoId");

                    b.Property<int>("AulaId");

                    b.HasKey("Id");

                    b.HasIndex("AlumnoId");

                    b.HasIndex("AulaId");

                    b.ToTable("AlumnoAula");
                });

            modelBuilder.Entity("m2m.Models.Aula", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Capacidad");

                    b.Property<string>("Nombre")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Aula");
                });

            modelBuilder.Entity("m2m.Models.AlumnoAula", b =>
                {
                    b.HasOne("m2m.Models.Alumno", "Alumno")
                        .WithMany("Aulas")
                        .HasForeignKey("AlumnoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("m2m.Models.Aula", "Aula")
                        .WithMany("Alumnos")
                        .HasForeignKey("AulaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
