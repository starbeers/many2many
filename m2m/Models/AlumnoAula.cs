﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace m2m.Models
{
    public class AlumnoAula
    {
        public int Id { get; set; }
        public int AlumnoId { get; set; }
        public int AulaId { get; set; }
        public Alumno Alumno { get; set; }
        public Aula Aula { get; set; }
    }
}
