﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace m2m.Models
{
    public class Aula
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        public int Capacidad { get; set; }

        public virtual ICollection<AlumnoAula> Alumnos {get; set;}
    }
}
